# Equations of Motion

Equations of motion of a spanning tree, subject to loop-closure forces
```math
\bm{M} \bm{v̇} + \bm{c} = \bm{K}^T \bm{λ} + \bm{τ}^i + \bm{τ}
```
where $`\bm{K}`$ is the constraint jacobian, $`\bm{λ}`$ is a vector of constraint forces and $`\bm{τ}^i`$ are internal (state dependent) and $`\bm{τ}`$ external joint torques. With loop-closure constraint equations
```math
\bm{K}\bm{v̇} = \bm{k} + \bm{k}_{stab}
\;,\quad \bm{K}(\bm{q}) ∈ \mathbb{R}^{n_c × n}
```
the spanning tree having $`n`$ degrees of freedom and $`n_c`$ the number of constraints imposed on the tree by loop-joints. Internal joint torques can be further separated into those resulting from elasticity $`\bm{τ}^e`$ and damping $`\bm{τ}^d`$.

## Solution to the constraint equation

Let us define $`r = \operatorname{rank}(\bm{K})`$, where
```math
\bm{K}\bm{v̇} = \bm{k} + \bm{k}_{stab}
```
If $`n = r`$ the constraint equation has a unique solution but if $`r < n`$ it has infinitely many solutions in the form
```math
\bm{v̇} = \bm{G} \bm{ẇ} + \bm{g}    
```
where $`\bm{ẇ} ∈ \mathbb{R}^{n_i}`$ is a vector of independent acceleration variables, matrix $`\bm{G} ∈ \mathbb{R}^{n × (n_i)}`$ satisfies
```math
\bm{K}\bm{G} = \bm{0}
```
and $`\bm{g}`$ is any solution of
```math
\bm{K}\bm{g} = \bm{k} + \bm{k}_{stab}
```
A method for calculating $`\bm{G}`$, $`\bm{g}`$ with $`\bm{ẇ}`$ chosen as a subset of tree-joint accelerations $`\bm{v̇}`$ is described [here](GgCalculation.md)

## Eliminating constraints
By pre multiplying the equations of motion by $`\bm{G}^T`$ and applying the substitution $`\bm{v̇} = \bm{G}\bm{ẇ} + \bm{g}`$ we may attain
```math
\bm{G}^T\bm{M}\,\bm{G}\,\bm{ẇ} + \bm{G}^T (\bm{M}\bm{g} + \bm{c} - \bm{τ}^i) = \bm{G}^T\bm{τ}
```
where constraint forces $`\bm{λ}`$ are eliminated and tree-joint accelerations are expressed as functions of independent acceleration variables $`\bm{ẇ}`$.

---

Assuming only a subset of tree joints is actuated by the input $`\bm{u}`$
```math
\bm{G}_A^T \bm{u}
=
\bm{G}^T \bm{τ}
\;,\quad 
\bm{u} ⊂ \bm{τ}
\,,\;
\bm{u} ∈ \mathbb{R}^{n_a}
```
we may re-write the equations of motion as
```math
\bm{G}^T\bm{M}\bm{G}
\,\bm{\dot{w}}
+
\bm{G}^T(\bm{M}\bm{g} + \bm{c} - \bm{τ}^i)
=
\bm{G}_A^T
\bm{u}
```
which can also be written in the compact form
```math
\bm{\tilde{M}} \bm{ẇ} + \bm{\tilde{c}} = \bm{G}_A^T \bm{u}
```
where $`\bm{\tilde{M}}(\bm{q},\bm{v}) ∈ \mathbb{R}^{n_i×n_i}`$ is the mass matrix and $`\bm{\tilde{c}}(\bm{q},\bm{v}) ∈ \mathbb{R}^{n_i}`$ the vector of bias terms expressed in independent coordinates.