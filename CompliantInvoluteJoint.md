# Compliant Involute Joint
Modified Involute joint with an additional degree of freedom along the involute.

Let us define points $`P`$ and $`S`$ as the origins of frames $`F_p`$ and $`F_s`$, the vector $`\bm{p}`$ as $`\overrightarrow{PS}`$
```math
\vphantom{p}^s\bm{p} = \bm{\hat{\jmath}}r + \bm{\hat{\imath}}(rq + d)
```
and $`\bm{E}`$ as the rotation matrix that transforms 3D vectors from $`F_p`$ to $`F_s`$ coordinates.
```math
\bm{E} = \bm{R}^T_z(q)
```

## Transformation
The transformation of spatial motion vectors between $`F_p`$ and $`F_s`$ consist of a rotation characterized by $`\bm{E}`$ followed by translation based on $`\vphantom{p}^s\bm{p}`$
```math
^s\bm{X}_p
=
\begin{bmatrix}
    \bm{1} & \bm{0} \\
    -\vphantom{p}^s\bm{p}× & \bm{1}
\end{bmatrix}
\begin{bmatrix}
    \bm{E} & \bm{0} \\
    \bm{0} & \bm{E}
\end{bmatrix}
=
\begin{bmatrix}
    \bm{E} & \bm{0} \\
    -\vphantom{p}^s\bm{p}×\bm{E} & \bm{E}
\end{bmatrix}
```

## Spatial velocity and motion subspace
the spatial *joint velocity* $`\bm{v}_J`$ with which the point $`S`$ moves in $`F_s`$ coordinates (in addition to $`\vphantom{X}^s\bm{X}_p \vphantom{v}^p\bm{v}_P`$) is comprised of the 3D vector *angular velocity* $`\bm{ω}`$ and *linear velocity* $`\bm{v}`$ of $`S`$ in relation to $`P`$ in the coordinates of $`F_s`$
```math
\bm{v}_J
=
\begin{bmatrix}
    \vphantom{p}^s\bm{ω} \\
    \vphantom{p}^s\bm{v}
\end{bmatrix}
=
\begin{bmatrix}
    \bm{ω} \\
    \bm{ω}×\vphantom{p}^s\bm{p} + \vphantom{p}^s\bm{\dot{p}}
\end{bmatrix}
```
where
```math
\bm{ω}×\vphantom{p}^s\bm{p} + \vphantom{p}^s\bm{\dot{p}}
=
\bm{\hat{k}}\dot{q} × (\bm{\hat{\jmath}}r + \bm{\hat{\imath}}rq + \bm{\hat{\imath}}d) + \bm{\hat{\imath}}r\dot{q} + \bm{\hat{\imath}}\dot{d}
=
-\bm{\hat{\imath}}\dot{q}r + \bm{\hat{\jmath}}\dot{q}rq + \bm{\hat{\jmath}}\dot{q}d + \bm{\hat{\imath}}r\dot{q} + \bm{\hat{\imath}}\dot{d}
=
\bm{\hat{\jmath}}\dot{q}(rq+d) + \bm{\hat{\imath}}\dot{d}
```
Therefore $`\bm{v}_J`$ simplifies to
```math
\bm{v}_J
=
\begin{bmatrix}
    \bm{\hat{k}}\dot{q} \\
    \bm{\hat{\jmath}}\dot{q}(rq+d) + \bm{\hat{\imath}}\dot{d}
\end{bmatrix}
```
and the *motion subspace* $`\bm{S}`$ takes the form
```math
\bm{S}
=
\begin{bmatrix}
	\bm{\hat{k}}            & \bm{0} \\
	\bm{\hat{\jmath}}(rq+d) & \bm{\hat{\imath}}
\end{bmatrix}
```

## Spatial acceleration and velocity product term
Spatial joint acceleration is defined as
```math
\bm{a}_J
=
\bm{\dot{v}}_J
=
\begin{bmatrix}
    \bm{\hat{k}}\ddot{q} \\
    \bm{\hat{\jmath}}\ddot{q}(rq+d) + \bm{\hat{\jmath}}\dot{q}(r\dot{q}+\dot{d}) + \bm{\hat{\imath}}\ddot{d}
\end{bmatrix}
```
which can be re-written using the *motion subspace* $`\bm{S}`$ and *velocity product term* $`\bm{c}_J`$
```math
\bm{a}_J
=
\bm{S}[ \ddot{q}\ \ddot{d} ]^T + \bm{c}_J
\;,\quad 
\bm{c}_J
=
\begin{bmatrix}
    \bm{0} \\
    \bm{\hat{\jmath}}\dot{q}(r\dot{q}+\dot{d})
\end{bmatrix}
```