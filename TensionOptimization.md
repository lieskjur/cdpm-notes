# Tension Optimization

In cable-driven mechanisms it is often desirable to maintain a certain degree of pre-tension in the cables. This tension is characterized by elasticity components $`\bm{τ}_e^i`$ in a subset of superfluous internal torques. As internal joint torques $`\bm{\tau}^i`$ are state dependent, $`\bm{τ}_e^i`$ cannot be directly solved for as part of the inverse dynamics problem. Instead an additional term(s) must be introduced for which we will optimize the input $`\bm{u}`$. Extracting only equations of motions pertaining to superfluous joints and separating $`\bm{\tilde{M}}_S\bm{v̇}_S`$ and $`\bm{τ}^i_S`$ to the left hand side we attain
```math
\bm{\tilde{M}}_S\bm{v̇} - \bm{τ}^i_S = \bm{G}_{AS}^T \bm{u} - \bm{G}_S^T (\bm{M}\bm{g} + \bm{c})
```
with the right hand side consisting of redistributed input torques and bias terms. These are either balanced out by joint torques $`\bm{τ}^i_S`$ or result in accelerations $`\bm{v̇}_S > \bm{0}`$ increasing the tension.


## Optimization constraints

Let us define $`\bm{ψ}_D`$ as the desired equivalent torques
```math
\bm{ψ}_D = \bm{\tilde{M}}_S\bm{v̇} - \bm{τ}^i_S
```
We may then constraint the input to
```math
\bm{ψ}_{D_{MIN}} ≤ \bm{G}_{AS}^T \bm{u} - \bm{G}_S^T(\bm{M}\bm{g}+\bm{c}) ≤ \bm{ψ}_{D_{MAX}}
```

Formulated as constrains of the QP problem described in [Input Optimization](InputOptimization.md) the terms will take the form
```math
\bm{a} ≡ \bm{ψ}_{D_{MIN}} - \bm{ψ}_2 \;,\quad 
\bm{A} ≡ \bm{G}_{AS}^T \bm{N} \;,\quad 
\bm{b} ≡ \bm{ψ}_{D_{MAX}} - \bm{ψ}_2
```
where
```math
\bm{ψ}_2 = \bm{G}_{AS}^T \bm{u}_2 - \bm{G}_S^T(\bm{M}\bm{g}+\bm{c})
\;,\quad 
\bm{N} = \operatorname{Null}(\bm{G}_{ar}^T)
```

## Alternative
Alternatively dampening components $`\bm{τ}^d`$ could be separated from $`\bm{τ}^e`$ and moved to the right hand side
```math
\bm{\tilde{M}}_S\bm{v̇} - \bm{τ}^e_S = \bm{G}_{AS}^T \bm{u} - \bm{G}_S^T (\bm{M}\bm{g} + \bm{c} - \bm{τ}^d)
```
In such a formulation damping components of internal joint torques are compensated by the input $`\bm{u}`$ increasing the rate at which the desired tension is achieved but resulting in greater oscillations.

To reduce vibrations, $`\bm{v̇}_S^T\bm{v̇}_S`$ could be added to the objective function.