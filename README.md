# Cable-Driven Parallel Manipulators - Notes

These notes pertain to the simulation and control of *cable-driven parallel manipulators* (CDPMs) which we will define as *closed-loop* mechanism with cable and pulley elements. This definition includes not only manipulators refered to as *cable-driven parallel robots* (CDPRs) but also *active tensegrities*.

## Dynamics and Control of Parallel Manipulators
- [Equations of Motion](EoM.md)
	- [Calculation of $`\bm{G}`$ and $`\bm{g}`$ for $`\bm{ẇ} ⊂ \bm{v̇}`$](GgCalculation.md)
- [Input Optimization](InputOptimization.md)
- Computed Torques
## Cable Manipulator Specific modifications
- Involute Joint
	- [Rigid](InvoluteJoint.md)
	- [Compliant](CompliantInvoluteJoint.md)
- Ramp elasticity
- [Reduced Dynamics](ReducedDynamics.md)
- [Maintaining Cable Tension](TensionOptimization.md)
