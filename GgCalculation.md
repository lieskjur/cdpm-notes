## Calculation of $`\bm{G}`$ and $`\bm{g}`$ for $`\bm{ẇ} ⊂ \bm{v̇}`$
To attain independent acceleration variables $`\bm{ẇ}`$ as a subset of $`\bm{v̇}`$ figuring in the constraint equation
```math
\bm{K}(\bm{G}\bm{ẇ}+\bm{g}) = \bm{k}
```
let us choose permutation matrices $`\bm{Q}_k`$ and $`\bm{Q}_q`$
```math
\begin{bmatrix}
	\bm{v̇}_D \\ \bm{v̇}_I
\end{bmatrix}
= \bm{Q}_q \bm{v̇}
\;,\quad 
\bm{Q}_k
\begin{bmatrix}
	\bm{k}_R \\ \bm{k}_S
\end{bmatrix}
=
\bm{k}
```
where
* $`\bm{v̇}_I ≡ \bm{ẇ} ∈ \mathbb{R}^{n-r}`$ - independent joint-tree variables
* $`\bm{v̇}_D ∈ \mathbb{R}^r`$ - dependent joint-tree variables
* $`\bm{k}_R ∈ \mathbb{R}^r`$ - reference constraint coefficients
* $`\bm{k}_S ∈ \mathbb{R}^{n_c-r}`$ superfluous constraint coefficients

giving us a permutation of the loop-closure constraint equation
```math
\bm{Q}_k
\begin{bmatrix}
	\bm{K}_{RD} & \bm{K}_{RI} \\
	\bm{K}_{SD} & \bm{K}_{SI}
\end{bmatrix}
\begin{bmatrix}
	\bm{v̇}_D \\ \bm{v̇}_I
\end{bmatrix}
=
\bm{Q}_k
\begin{bmatrix}
	\bm{k}_R \\ \bm{k}_S
\end{bmatrix}
```
where
```math
\begin{bmatrix}
	\bm{K}_{RD} & \bm{K}_{RI} \\
	\bm{K}_{SD} & \bm{K}_{SI}
\end{bmatrix}
=
\bm{Q}_k^T \bm{K} \bm{Q}_q^T
```

---

If $`\bm{Q}_q`$ and $`\bm{Q}_k`$ are chosen so that $`\bm{K}_{RD}`$ is invertible for all configurations of the manipulator, $`\bm{G}`$ and $`\bm{g}`$ take the form
```math
\bm{G}
=
\bm{Q}_q^T
\begin{bmatrix}
	- \bm{K}_{RD}^{-1} \bm{K}_{RI} \\ \bm{1}
\end{bmatrix}
\;,\quad 
\bm{g}
=
\bm{Q}_q^T
\begin{bmatrix}
	\bm{K}_{RD}^{-1} \bm{k}_R \\ \bm{0}
\end{bmatrix}
```
for the chosen set $`\bm{ẇ} ≡ \bm{v̇}_I ⊂ \bm{v̇}`$.

### Algorithm
In the resulting algorithm we may avoid matrix multiplication as means of permutation by working with sub-arrays of $`\bm{K}`$ and $`\bm{k}`$<!--  ,filling $`\bm{G}`$ with `1` in a loop -->. Neccessary for this approach is to store indices of *independent joint-space velocities* `I` and *dependent joint-space velocities* `D`. 
```julia
function G(K)
	G = zeros(n,n-r)
	for (i,j) in enumerate(I)
		G[j,i] = 1
	end
	G[D,:] = -K[R,D]\K[R,I]
end
```

```julia
function g(K,k)
	g = zeros(n)
	g[D] = K[R,D]\k[R]
end
```
If *Reference joint-space variables* `R` are determined on a per-calulation basis using the algorithm describe above
```julia
```
we may also utilize the LU decomposition to increace performace by replacing `K[R,D]\M` with `U\(L\M)` where `M` represents `K[R,I]` and `k[R]` during the calculation of $`\bm{G}`$ and $`\bm{g}`$ respectively. 