# Reduced Dynamics
Let us assume *EoM* in the form
```math
\bm{G}^T\bm{M}\bm{G}
\,\bm{\dot{w}}
+
\bm{G}^T(\bm{M}\bm{g} + \bm{c} - \bm{τ}^i)
=
\bm{G}_A^T
\bm{u}
```
Using a permutation matrix $`\bm{Q}_{RS}`$ we may separate columns of $`\bm{M}`$ and elements of $`\bm{v̇}`$ by attribute, resulting in
```math
\begin{bmatrix}
	\bm{\tilde{M}}_{R} & \bm{\tilde{M}}_{S} \\
\end{bmatrix}
\begin{bmatrix}
	\bm{\dot{v}}_R \\
	\bm{\dot{v}}_S
\end{bmatrix}
+
\bm{G}^T(\bm{M}\bm{g} + \bm{c} - \bm{τ}^i)
=
\bm{G}_A^T
\bm{u}
```
where
```math
\begin{bmatrix}
	\bm{\tilde{M}}_{R} & \bm{\tilde{M}}_{S} \\
\end{bmatrix}
=
\bm{G}^T\bm{M}\,\bm{G}\,\bm{Q}_{RS}^T
\;,\quad 
\begin{bmatrix}
	\bm{\dot{v}}_R \\
	\bm{\dot{v}}_S
\end{bmatrix}
=
\bm{Q}_{RS}\,\bm{ẇ}
```

---
As $`\bm{τ}_U = \bm{0}`$ are external torques at unactuated joints, they as well as $`\bm{G}_U^T`$ do not figure in the equations of motion.

Under certain conditions we may assume that $`∀t: \bm{v̇}_S(t) → \bm{0}`$ which allows us to eliminate *superfluous* equations and columns of $`\tilde{M}`$.
```math
\bm{\tilde{M}}_{RR}
\bm{\dot{v}}_R
+
\bm{G}_R^T(\bm{M}\bm{g} + \bm{c} - \bm{τ}^i)
=
\bm{G}_A^T
\bm{u}
```
where
```math
\begin{bmatrix}
	\bm{M}_{RR}^T \\ \bm{M}_{SR}^T
\end{bmatrix}
=
\bm{Q}_{RS}\,\bm{M}_R^T
\;,\quad 
\begin{bmatrix}
	\bm{G}_{R}^T \\ \bm{G}_{S}^T
\end{bmatrix}
=
\bm{Q}_{RS}\,\bm{G}^T
```