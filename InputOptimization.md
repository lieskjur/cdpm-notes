# Optimization for Minimal Input
In over-actuated parallel mechanisms the *EoM* can be expressed as
```math
\bm{\tilde{M}} \bm{ẇ} + \bm{\tilde{c}} = \bm{G}_A^T \bm{u}
```
where $`\bm{G}_A ∈ \mathbb{R}^{n_i×n_a} ,\ n_a>n_i`$. This presents us with the possibility of input optimizations e.i. selecting inputs (subject to constraints) such that an *objective* function is minimized. Here we will formulate the *[constrained optimization](https://en.wikipedia.org/wiki/Constrained_optimization) problem* (COP) and [*quadratic programing*](https://en.wikipedia.org/wiki/Quadratic_programming) (QP) problem of minimizing the *objective function*
```math
\bm{u}^T\bm{u} \;,\quad \bm{u} ∈ ⟨\bm{u}_{MIN},\bm{u}_{MAX}⟩
```
while also satisfying the EoM. In [link to next section] we will modify/add criteria specific to cable actuated manipulators.

## COP formulation
A *constrained optimization problem* may be written as
```math
\text{minimize:} \quad f(\bm{x}) \\
\text{subject to:} \quad \bm{g}(\bm{x}) = \bm{c} \;,\quad \bm{h}(\bm{x}) ≥ \bm{d}
```
Problems formulated as COP are separated based on algorithms used for their solution into
- *Linear programing* (LP) problems where $`f(\bm{x})`$, $`\bm{g}(\bm{x})`$ and $`\bm{h}(\bm{x})`$ are linear functions
- *Non-linear programing* (NLP) problems where $`f(\bm{x})`$, $`\bm{g}(\bm{x})`$ and $`\bm{h}(\bm{x})`$ are non-linear functions

source: [wikipedia](https://en.wikipedia.org/wiki/Constrained_optimization)

---

If using the COP formulation of our problem we will optimize for $`\bm{x} ≡ \bm{u}`$ with the *objective function*
```math
f(\bm{x}) ≡ \bm{u}^T\bm{u} \;,\quad 
```
*equality constraints*
```math
\bm{g}(\bm{x}) ≡ \bm{G}_A^T \bm{u} \;,\quad 
\bm{c} ≡ \bm{\tilde{M}} \bm{ẇ} + \bm{\tilde{c}} \;,\quad
```
and *inequality constraints* 
```math
\bm{h}(\bm{x}) ≡ \begin{bmatrix} \bm{u} \\ -\bm{u} \end{bmatrix} \;,\quad 
\bm{d} ≡ \begin{bmatrix} \bm{u}_{MIN} \\ -\bm{u}_{MAX} \end{bmatrix}
```
The *objective function* being quadratic, NLP algorithms will be used for the optimization.

## QP formulation
A *quadratic programing* problem can be stated as
```math
\text{minimize:} \quad \frac{1}{2}\bm{x}^T\bm{P}\bm{x} + \bm{q}^T\bm{x} \\ 
\text{subject to:} \quad \bm{a} ≤ \bm{A}\bm{x} ≤ \bm{b}
```
source: [OSQP](https://osqp.org/docs/index.html#])

---

In order to solve our problem formulated as a QP, first an input satisfying in EoM must be calculated e.g.
```math
\bm{u}_2 = (\bm{G}_A^T)^+\,\bm{G}^T (\bm{\tilde{M}} \bm{ẇ} + \bm{\tilde{c}})
```
as well as the null matrix $`\bm{N} = \operatorname{Null}(\bm{G}_A^T)`$ where
```math
∀\bm{z} ∈ \mathbb{R}^{n_a-n_i} : \quad \bm{G}_A^T\bm{N} \bm{z} = \bm{0}
```
Consequently we will optimize for $`\bm{x} ≡ \bm{z}`$ with *objective function* terms
```math
\bm{P} ≡ \bm{N}^T\bm{N} \;,\quad 
\bm{q} ≡ \bm{N}^T\bm{u}_2
```
and *constraints'* terms
```math
\bm{a} ≡ \bm{u}_{MIN}-\bm{u}_2 \;,\quad 
\bm{A} ≡ \bm{N} \;,\quad 
\bm{b} ≡ \bm{u}_{MAX}-\bm{u}_2
```

## Performance

While formulating the COP is more straight forward than the QP problem, specialized QP algorithms out-perform NLP algorithms even when factoring in preliminary calculations. An added advantage of the QP formulation is that the number of optimized variables is equal to the degree of actuation redundancy which is generally small.

so far tested only for small matrices:
* n_a=4, n_i=2:
	* QP including preliminary calculations ($`t≈15μs`$) approx. 45x faster than NLP ($`t≈600μs`$).
